# Configure the AWS Provider
#Never hard-code in the configuration file, it's not encrypted. 
#Provider is a code that knows how to talk to AWS
#Terraform provider block is optional


#Only work with Terraform providers in Hashicorp registry, do not work for Linode Provider

provider "aws" {
  region = "us-east-1"
  access_key = "AKIAWOGITRWIGCMWGLNP"
  secret_key = "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"
}


#resources block starts with provider alias
#refer to documentation for more information
# resource name is a variable - byterites_network-dev

variable "subnet_cidr_block" {
  description = "The subnet address to use for our custom VPC"
}


resource "aws_vpc" "byterites_network-dev" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "byterites_network"
  }
}

#vpc id for a subnet references the vpc alias created
#I named this subnet dev-subnet-1
#Using availability_zone to ensure my subnets are not scattered all across the AZs

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.byterites_network-dev.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = "us-east-1a"

  tags = {
    Name = "dev-subnet-1"
  }
}

output "dev-subnet-outputs-ids" {
  value = aws_subnet.dev-subnet-1.id
}


